// TextToSpeech.cpp : Defines the entry point for the console application.
//

//Additional includes in stdafx.h to enable TTS

#include "stdafx.h"
#include <iostream>
#include <sapi.h>
#include <string.h>
#include <string>

using namespace std;


int main(int argc, char* argv[])
{
	//Useful for checking errors
	//This ensures an invalid pointer is not reused or reminder that the pointer has been allocated/deallocated
	ISpVoice * pVoice = NULL;

	//SAPI is a COM based app
	//COM must be initialised before use and during SAPI active time
	if (FAILED(::CoInitialize(NULL)))
    return FALSE;
	
	//Create instance of a voice
	//Voice is a COM object
	//SAPI assigns most values automatically so it can be used immediately 
	//All defaults may be changed programmatically or in Speech properties in control panel
	HRESULT hr = CoCreateInstance(CLSID_SpVoice, NULL, CLSCTX_ALL, IID_ISpVoice, (void **)&pVoice);
	if (SUCCEEDED(hr)) {

		//Default speech
		//Holds a wide character
		//2 bytes on Windows
		wstring text;
		cout << "Enter desired text:\n" << endl;
		//Reads users text that they wish to have spoken back to them
		getline(wcin, text);

		//Converts the text char variable into an array of characters i.e a string 
		//Includes a terminating null character at the end
		hr = pVoice->Speak(text.c_str(), 0, NULL);

		//Change pitch 
		//hr = pVoice->Speak(L"This sounds normal <pitch middle = '-10' /> but the pitch drops half way through", SPF_IS_XML, NULL);
		pVoice->Release();
		pVoice = NULL;

	}

	::CoUninitialize();
	return TRUE;
}



